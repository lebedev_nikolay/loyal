#!/bin/bash
docker stop postgresql-master 
docker rm postgresql-master 
docker volume rm pgsql_master3
rm -rf /var/lib/docker/volumes/pgsql_master3/_data/*
docker volume create --name=pgsql_master3

docker run --name postgresql-master -h postgresql-master -e REPUSER=replicator -e REPPASS=123456 -e POSTGRES_DB=test -e POSTGRES_USER=test  -e POSTGRES_PASSWORD=123456  -v pgsql_master3:/var/lib/postgresql/data -d  posql:9.6	

#echo "host replication replicator 0.0.0.0/0 trust" >> /var/lib/docker/volumes/pgsql_master3/_data/pg_hba.conf

#docker restart postgresql-master
