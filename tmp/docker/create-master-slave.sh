#!/bin/bash

docker stop postgresql-master postgresql-slave
docker rm postgresql-master postgresql-slave -f

rm -rf /home/rocot/docker_share/pgdata/master/*
rm -rf /home/rocot/docker_share/pgdata/slave/*
rm -rf /var/lib/docker/volumes/pgsql_master/_data/*
rm -rf /var/lib/docker/volumes/pgsql_slave/_data/*

docker volume rm pgsql_master
docker volume rm pgsql_slave 

docker volume create --name=pgsql_master
docker volume create --name=pgsql_slave

docker run --name postgresql-master -h postgresql-master -e REPUSER=replicator -e REPPASS=123456 -e POSTGRES_DB=test -e POSTGRES_USER=test  -e POSTGRES_PASSWORD=123456  -v pgsql_master:/var/lib/postgresql/data -d  posql:9.6	

docker exec --privileged -u root -d postgresql-master sh echo "#!/bin/bash" > /docker-entrypoint.sh
docker exec --privileged -u root -d postgresql-master sh echo "echo the text" >> /docker-entrypoint.sh

echo "host replication replicator 0.0.0.0/0 trust" >> /var/lib/docker/volumes/pgsql_master/_data/pg_hba.conf

cat << EOF >> /var/lib/docker/volumes/pgsql_master/_data/postgresql.conf

wal_level = hot_standby

min_wal_size = 2

max_wal_size = 8

max_wal_senders = 3

wal_keep_segments = 8

hot_standby = on

EOF

docker restart postgresql-master

docker run -it --rm --link postgresql-master:db -e PGPASSWORD=123456 postgres sh -c "psql -h postgresql-master test -Utest -c \"CREATE USER replicator REPLICATION LOGIN ENCRYPTED PASSWORD 'replpass';\""

docker run -e POSTGRES_USER=test -e POSTGRES_PASSWORD=123456 -v pgsql_master:/var/lib/postgresql/data --link postgresql-master:db --rm -it postgres sh -c 'pg_basebackup -h postgresql-master -D /var/lib/postgresql/data -U replicator -P -v -x'

chmod 700 /var/lib/docker/volumes/pgsql_slave/_data

cat << EOF > /var/lib/docker/volumes/pgsql_slave/_data/recovery.conf

primary_conninfo = 'host=postgresql-master port=5432 user=replicator password=123456'

trigger_file = '/var/lib/postgresql/data/failover'

standby_mode = 'on'

EOF

docker run --name postgresql-slave -e REPUSER=replicator -e REPPASS=123456 -e POSTGRES_USER=test -e POSTGRES_PASSWORD=123456 -v pgsql_slave:/var/lib/postgresql/data/sdb --link postgresql-master:db -d posql:9.6


docker cp /home/rocot/loyal/docker/PostgreSQL/sql_config.sh postgresql-master:/tmp
docker exec --privileged -u postgres -d postgresql-master chmod +x /tmp/sql_config.sh
docker exec --privileged -u postgres -d postgresql-master sh /tmp/./sql_config.sh








