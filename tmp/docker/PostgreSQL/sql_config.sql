CREATE TABLE Users (
    UserID           serial PRIMARY KEY,
    FullName         VARCHAR (50) UNIQUE NOT NULL,          
    Login            VARCHAR (20) UNIQUE NOT NULL,           
    HashPassword     VARCHAR (50) UNIQUE NOT NULL, 
    Email            VARCHAR (20) UNIQUE NOT NULL, 
    PhoneNumber      VARCHAR (20) UNIQUE NOT NULL
);

CREATE TABLE Permission (
    PermissionID          serial PRIMARY KEY,
    Name        VARCHAR (20) UNIQUE NOT NULL
);

CREATE TABLE UserPermission (
    UserPermissionID       serial PRIMARY KEY,
    UserID                 integer NOT NULL,
    PermissionID           integer NOT NULL,
	CONSTRAINT UserPermission_UserID_fkey FOREIGN KEY (UserID)
      REFERENCES Users(UserID) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION,
    CONSTRAINT UserPermission_PermissionID_fkey FOREIGN KEY (PermissionID)
      REFERENCES Permission (PermissionID) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE USER admin_user WITH password 'qwerty';
GRANT ALL privileges ON DATABASE test TO admin_user;

CREATE USER create_user WITH password 'qwerty';
GRANT CREATE ON DATABASE test TO create_user;

INSERT INTO Users (UserID, FullName, Login, HashPassword, Email, PhoneNumber) VALUES (1, 'Nikolay Lebedev', 'bolololo', 'fff8f979d9f9f79f7f', 'gtgt@mail.ru', '8-921-954-84-58');
INSERT INTO Users (UserID, FullName, Login, HashPassword, Email, PhoneNumber) VALUES (2, 'Nikolay Lebedev2', 'bolololo2', 'fff8f979d9f9f79f7f2', '2gtgt@mail.ru', '8-921-954-84-52');
INSERT INTO Users (UserID, FullName, Login, HashPassword, Email, PhoneNumber) VALUES (3, 'Nikolay Lebedev3', 'bolololo3', 'fff8f979d9f9f79f7f3', '3gtgt@mail.ru', '8-921-954-84-53');

INSERT INTO Permission (PermissionID, Name ) VALUES (1, 'Nikolay Lebedev1');
INSERT INTO Permission (PermissionID, Name ) VALUES (2, 'Nikolay Lebedev2');

