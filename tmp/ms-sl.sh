#!/bin/bash

docker stop psql-slave psql-master
docker rm   psql-slave psql-master -f

docker run --name='psql-master' -h psql-master -d \
  -e 'PG_MODE=master' -e 'PG_TRUST_LOCALNET=true' \
  -e 'REPLICATION_USER=replicator' -e 'REPLICATION_PASS=123456' \
  -e 'DB_NAME=test' -e 'DB_USER=test' -e 'DB_PASS=123456' \
 romeoz/docker-postgresql:9.5

sleep 5;

docker run --name='psql-slave' -h psql-slave -d  \
  -e 'PG_MODE=slave' -e 'PG_TRUST_LOCALNET=true' \
  -e 'REPLICATION_HOST=psql-master' -e 'REPLICATION_PORT=5432' \
  -e 'REPLICATION_USER=replicator' -e 'REPLICATION_PASS=123456' \
  --link psql-master:db \
 romeoz/docker-postgresql:9.5
 
docker cp /home/rocot/loyal/docker/pssql-master/sql_config.sh psql-master:/tmp
docker exec --privileged -u postgres -d psql-master chmod +x /tmp/sql_config.sh
docker exec --privileged -u postgres -d psql-master sh /tmp/./sql_config.sh

