#!/bin/bash

cd /tmp
git clone https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/loyal.git
cd /tmp/loyal/ansible

apt-get install -y python-apt

ansible-playbook -i hosts default.yml

docker exec --privileged -u postgres -d psql-master sh /tmp/./sql_config.sh
