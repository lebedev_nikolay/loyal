#!/bin/bash

cd /tmp

git clone https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/loyal.git

cd loyal

HOME_DIR=.

docker stop haproxy psql-slave psql-master  python-web-1  python-web-2
docker rm   haproxy psql-slave psql-master  python-web-1  python-web-2 -f
sleep 5;

docker run --name='psql-master' -h psql-master -d \
  -e 'PG_MODE=master' -e 'PG_TRUST_LOCALNET=true' \
  -e 'REPLICATION_USER=replicator' -e 'REPLICATION_PASS=123456' \
  -e 'DB_NAME=test' -e 'DB_USER=test' -e 'DB_PASS=123456' \
 romeoz/docker-postgresql:9.5

sleep 5;



#Изменение файла sql_config.sh меняем имя и базу

docker cp $HOME_DIR/docker/pssql-master/sql_config.sh psql-master:/tmp
docker exec --privileged -u postgres -d psql-master chmod +x /tmp/sql_config.sh
docker exec --privileged -u postgres -d psql-master sh /tmp/./sql_config.sh

sleep 5;

docker run --name='psql-slave' -h psql-slave -d  \
  -e 'PG_MODE=slave' -e 'PG_TRUST_LOCALNET=true' \
  -e 'REPLICATION_HOST=psql-master' -e 'REPLICATION_PORT=5432' \
  -e 'REPLICATION_USER=replicator' -e 'REPLICATION_PASS=123456' \
  --link psql-master:db \
 romeoz/docker-postgresql:9.5

# Добавить код проверки slave

#Сборка и запуск web серверов

cd $HOME_DIR/docker/flask
docker build -t flask-ununtu14 .
docker run --name python-web-1 -d -p 5000:5000  --link psql-master:db flask-ununtu14
docker run --name python-web-2 -d -p 5001:5001  --link psql-master:db flask-ununtu14

#Сборка haproxy

web_ip_1=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' python-web-1);
web_ip_2=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' python-web-2);

#Изменение параметров haproxy
cp $HOME_DIR/docker/haproxy/haproxy.cfg $HOME_DIR/docker/haproxy/haproxy.cfg.bk
sed -i "s/ip_1/$web_ip_1/g" $HOME_DIR/docker/haproxy/haproxy.cfg
sed -i "s/ip_2/$web_ip_2/g" $HOME_DIR/docker/haproxy/haproxy.cfg
#Настройка ограничения по ip haproxy
sed -i "s/allow_list_ip/127.0.0.1 192.168.1.55 172.17.0.9/g" $HOME_DIR/docker/haproxy/haproxy.cfg   
#Проверка корректности файла конфигурации

cd $HOME_DIR/docker/haproxy/
docker build -t haproxy15 .
docker run -d --name haproxy -h haproxy haproxy15
mv $HOME_DIR/docker/haproxy/haproxy.cfg.bk $HOME_DIR/docker/haproxy/haproxy.cfg

ha_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' haproxy);

sleep 5;

curl  $ha_ip

curl http://$ha_ip/stats

echo

#Настройка iptables

#apt-get remove ufw -f

#sudo iptables-save >> /tmp/iprules.txt
#sudo iptables-restore <  /tmp/iptables.txt

#iptables -n -L -v --line-numbers
#iptables -A OUTPUT -j ACCEPT
#iptables -P INPUT ACCEPT
#iptables -P FORWARD DROP
#iptables -A INPUT -m state --state ESTABLISHED,RELATED  -p tcp -m multiport --dports 80,22 -j ACCEPT
#iptables -A INPUT -p tcp --syn --dport 80 -m connlimit --connlimit-above 5 --connlimit-mask 24 -j DROP
#iptables -A INPUT -p udp --sport 53 -j ACCEPT
#iptables -A INPUT -p TCP --dport 80 -j ACCEPT
#iptables -A INPUT -j DROP


#iptables -A INPUT -p TCP --dport 22 -j ACCEPT



#iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A INPUT -m state --state ESTABLISHED,RELATED  -p tcp -m multiport --dports 80,22 -j ACCEPT

#
#iptables -A INPUT -i lo -j ACCEPT
#iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A INPUT -p tcp --sport 80 -j ACCEPT
#iptables -A INPUT -p udp --sport 53 -j ACCEPT
#iptables -A INPUT -j DROP

#iptables-save > /etc/iptables.rules




#Настройка досупа к haproxy stats acl white_list src 127.0.0.1 192.168.1.205    tcp-request content accept if white_list
#sudo apt-get install openssh-server






