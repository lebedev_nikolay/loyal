#!/bin/bash
HOME_DIR=.

docker stop haproxy psql-slave psql-master  python-web-1  python-web-2
docker rm   haproxy psql-slave psql-master  python-web-1  python-web-2 -f
sleep 2;

echo 'done clear'
docker run --name='psql-master' -h psql-master -d \
  -e 'PG_MODE=master' -e 'PG_TRUST_LOCALNET=true' \
  -e 'REPLICATION_USER=replicator' -e 'REPLICATION_PASS=123456' \
  -e 'DB_NAME=test' -e 'DB_USER=test' -e 'DB_PASS=123456' \
 romeoz/docker-postgresql:9.5

#Изменение файла sql_config.sh меняем имя и базу

docker cp $HOME_DIR/docker/pssql-master/sql_config.sh psql-master:/tmp
docker exec --privileged -u postgres -d psql-master chmod +x /tmp/sql_config.sh
docker exec --privileged -u postgres -d psql-master sh /tmp/./sql_config.sh


echo 'done sql'

cd $HOME_DIR/docker/flask
docker build -t flask-ununtu14 .
docker run --name python-web-1 -d -p 5000:5000  --link psql-master:db flask-ununtu14

echo 'done site'

docker run -d --name rancher_server --restart=unless-stopped -p 8080:8080 rancher/server

echo 'rancher_server http://site:8080 and add agent'





